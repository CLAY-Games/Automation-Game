using Clay.Base;
using Clay.Input;
using Clay.UI.DevPanel;

using UnityEngine;
using UnityEngine.InputSystem;

namespace Clay
{
    public class DevPanel : MonoBehaviour
    {
        [SerializeField] private UnitSpawner         playerUnitSpawner;
        [SerializeField] private UnitSpawner         enemyUnitSpawner;
        [SerializeField] private CameraRigController cameraRigController;

        private IDevPanelUiConnector uiConnector;

        [SerializeField] private InputManager inputManager;

        private bool isEnabled;

        private void Awake()
        {
            uiConnector = GetComponent<IDevPanelUiConnector>();

            uiConnector.TimeScaleChanged += value => Time.timeScale = value;
            uiConnector.SpawnPlayerUnit += () => playerUnitSpawner.SpawnUnitInstantly();
            uiConnector.SpawnEnemyUnit += () => enemyUnitSpawner.SpawnUnitInstantly();
            uiConnector.CameraPanToggleChanged += newValue => cameraRigController.enablePanByMouseAtBorder = newValue;

            inputManager.DeselectAll += OnDeselectAll;
            inputManager.OnToggleDevPanel += OnToggleDevPanel;
        }

        private void Start()
        {
            uiConnector.DisableUi();
        }

        private void OnDeselectAll(InputAction.CallbackContext ctx)
        {
            isEnabled = false;
            uiConnector.DisableUi();
        }

        private void OnToggleDevPanel(InputAction.CallbackContext ctx)
        {
            isEnabled = !isEnabled;
            if (isEnabled)
            {
                uiConnector.EnableUi();
            }
            else
            {
                uiConnector.DisableUi();
            }
        }
    }
}
