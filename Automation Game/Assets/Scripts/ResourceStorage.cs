﻿using System;

using Clay.UI.Resource;
using Clay.Unit;

using UnityEngine;

using ResourceType = Clay.Helper.ResourceType;

namespace Clay
{
    // Resource Storage for holding a single type of resource
    [RequireComponent(typeof(IResourceUi))]
    public class ResourceStorage : MonoBehaviour
    {
        public ResourceType Type;

        // TODO rewrite to access using enum identifiers

        [SerializeField] private int capacity;
        [SerializeField] private int currentFill;

        private IResourceUi ui;
        
        public void EnableUi()
        {
            ui.Enable();
        }

        public int Capacity
        {
            get => capacity;
            private set
            {
                ui.Capacity = value;
                capacity = value;
                OnCapacityChanged?.Invoke(value);
            }
        }

        public int CurrentFill
        {
            get => currentFill;
            private set
            {
                ui.Amount = value;
                currentFill = value;
                OnCurrentFillChanged?.Invoke(value);
            }
        }

        public event Action<int> OnCapacityChanged;
        public event Action<int> OnCurrentFillChanged;
        public event Action      OnStorageWasEmptied;


        /// <returns>The amount of resources that could be successfully withdrawn</returns>
        public (ResourceType type, int amount) RequestResources(int amount)
        {
            int newFill = CurrentFill - amount;
            if (newFill >= 0)
            {
                CurrentFill = newFill;
                return (this.Type, amount);
            }

            int remainingFill = CurrentFill;
            CurrentFill = 0;
            OnStorageWasEmptied?.Invoke();

            return (this.Type, remainingFill);
        }

        private void Awake()
        {
            ui = GetComponent<IResourceUi>();
        }

        private void Start()
        {
            ui.ResourceType = Type;
            ui.Capacity = Capacity;
            ui.Amount = currentFill;
            ui.Disable();
        }
    }
}
