﻿using UnityEngine;

namespace Clay
{
    public interface IUnitInteractable
    {
        public GameObject GetRoot();
    }
}
