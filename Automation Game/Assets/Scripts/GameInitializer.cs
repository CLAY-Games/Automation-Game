﻿using System;

using Clay.Colony;

using UnityEditor;

using UnityEngine;

namespace Clay
{
    public class GameInitializer : MonoBehaviour
    {
        [SerializeField] private Colony.Colony playerColony;
        [SerializeField] private Colony.Colony enemyColony;

        [SerializeField] private ColonyResourceStorage playerResourceStorage;
        [SerializeField] private ColonyResourceStorage enemyResourceStorage;

        [SerializeField] private int DefaultStorageCapacity = 3000;

        [SerializeField] private int BlotriumStartAmount = 600;
        [SerializeField] private int CaspiumStartAmount  = 250;

        public void InitializeGame()
        {
            InitializeResourceStorage(playerResourceStorage);
            InitializeResourceStorage(enemyResourceStorage);

            enemyColony.ResetCurrentUnits();
            playerColony.ResetCurrentUnits();
        }

        private void InitializeResourceStorage(ColonyResourceStorage resourceStorage)
        {
            resourceStorage.BlotriumAmount = BlotriumStartAmount;
            resourceStorage.CaspiumAmount = CaspiumStartAmount;
            resourceStorage.CaspiumCapacity = DefaultStorageCapacity;
            resourceStorage.BlotriumCapacity = DefaultStorageCapacity;
        }

        private void Awake()
        {
            InitializeGame();
        }
    }
}
