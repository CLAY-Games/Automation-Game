﻿using Clay.Selection;
using Clay.Unit;

using UnityEngine;

namespace Clay
{
    public class ResourceStorageUnitInteractable : SelectionClient, IUnitInteractable, IClickable
    {
        [SerializeField] ResourceStorage  resourceStorage;

        public GameObject GetRoot()
        {
            return resourceStorage.gameObject;
        }

        public void Click()
        {
            TakeSelection();
            resourceStorage.EnableUi();
        }
    }
}
