using Clay.Helper;

using UnityEngine;
using UnityEngine.Events;

namespace Clay.Colony
{
    [CreateAssetMenu(fileName = "ColonyResourceStorage.asset", menuName = "ScriptableObjects/ColonyResourceStorage")]
    public class ColonyResourceStorage : ScriptableObject
    {
        public event UnityAction<int> OnCaspiumCapacityChanged; 
        public event UnityAction<int> OnCaspiumAmountChanged;
    
        public event UnityAction<int> OnBlotriumCapacityChanged; 
        public event UnityAction<int> OnBlotriumAmountChanged;
    
        [SerializeField] private int caspiumCapacity;
        [SerializeField] private int caspiumAmount;
        [SerializeField] private int blotriumCapacity;
        [SerializeField] private int blotriumAmount;

        public int CaspiumCapacity
        {
            get => caspiumCapacity;
            set
            {
                OnCaspiumCapacityChanged?.Invoke(value);
                caspiumCapacity = value;
            }
        }

        public int CaspiumAmount
        {
            get => caspiumAmount;
            set
            {
                OnCaspiumAmountChanged?.Invoke(value);
                caspiumAmount = value;
            }
        }

        public int BlotriumCapacity
        {
            get => blotriumCapacity;
            set
            {
                OnBlotriumCapacityChanged?.Invoke(value);
                blotriumCapacity = value;
            }
        }

        public int BlotriumAmount
        {
            get => blotriumAmount;
            set
            {
                OnBlotriumAmountChanged?.Invoke(value);
                blotriumAmount = value;
            }
        }
    
    

        /// <summary>
        /// Takes the requested amount of resources, if there are enough resources available. Otherwise none are taken.
        /// </summary>
        /// <returns>True if the requested amount of resources could be taken</returns>
        public bool GetAllResourcesOrNone(int amount, ResourceType resourceType)
        {
            var currentFill = resourceType == ResourceType.Blotrium ? blotriumAmount : caspiumAmount;
            if (currentFill < amount)
            {
                return false;
            }

            currentFill -= amount;
            if (resourceType == ResourceType.Blotrium)
            {
                BlotriumAmount = currentFill;
            }
            else
            {
                CaspiumAmount = currentFill;
            }

            return true;
        }
    }
}
