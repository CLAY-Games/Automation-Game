using System;

using Clay.UI.ColonyOverlay;

using UnityEngine;

namespace Clay.Colony
{
    [RequireComponent(typeof(IColonyOverlayUiConnector))]
    public class ColonyOverlay : MonoBehaviour
    {
        [SerializeField] private Colony                colony;
        [SerializeField] private ColonyResourceStorage colonyResourceStorage;

        private IColonyOverlayUiConnector uiConnector;

        private void Awake()
        {
            uiConnector = GetComponent<IColonyOverlayUiConnector>();
        }

        private void Start()
        {
            InitializeUi();
            RegisterEvents();
        }

        private void InitializeUi()
        {
            uiConnector.BlotriumAmount = colonyResourceStorage.BlotriumAmount;
            uiConnector.CaspiumAmount = colonyResourceStorage.CaspiumAmount;
            uiConnector.UnitsAmount = colony.CurrentUnits;
            uiConnector.MaxUnits = colony.MaxUnits;
        }


        private void RegisterEvents()
        {
            colony.OnCurrentUnitsChanged += unitCount => uiConnector.UnitsAmount = unitCount;
            colony.OnMaxUnitsChanged += unitCount => uiConnector.MaxUnits = unitCount;

            colonyResourceStorage.OnBlotriumAmountChanged += amount => uiConnector.BlotriumAmount = amount;
            colonyResourceStorage.OnCaspiumAmountChanged += amount => uiConnector.CaspiumAmount = amount;
        }
    }
}
