﻿using System;
using System.Collections.Generic;

using Clay.Base;

using UnityEngine;
using UnityEngine.Events;

namespace Clay.Colony
{
    [CreateAssetMenu(fileName = "Colony.asset", menuName = "ScriptableObjects/Colony")]
    public class Colony : ScriptableObject
    {
        public UnityAction<int> OnCurrentUnitsChanged;
        public UnityAction<int> OnMaxUnitsChanged;

        [SerializeField] private int maxUnits = 4;
        [SerializeField] private int currentUnits;

        [SerializeField] private ColonyResourceStorage resourceStorage;

        public List<Unit.Unit> units = new();

        public int CurrentUnits
        {
            get => currentUnits;
            private set
            {
                currentUnits = value;
                OnCurrentUnitsChanged?.Invoke(value);
            }
        }

        public int MaxUnits
        {
            get => maxUnits;
            private set
            {
                maxUnits = value;
                OnMaxUnitsChanged?.Invoke(value);
            }
        }

        /// <summary>
        /// Takes a unit slot and subtracts the blotriumCost from the amount in the resource storage.
        /// Only does so if both actions are possible.
        /// </summary>
        /// <returns>True when both actions were done successfully.</returns>
        public bool RequestUnit(int blotriumCost)
        {
            if (currentUnits >= maxUnits)
            {
                return false;
            }

            if (resourceStorage.BlotriumAmount < blotriumCost)
            {
                return false;
            }

            resourceStorage.BlotriumAmount -= blotriumCost;
            CurrentUnits++;
            return true;
        }

        public void RegisterUnit(Unit.Unit unit)
        {
            units.Add(unit);
        }
        
        public void OnUnitDestroyed(Unit.Unit unit)
        {
            units.Remove(unit);
            CurrentUnits--;
        }

        public void ResetCurrentUnits()
        {
            CurrentUnits = 0;
        }
    }
}
