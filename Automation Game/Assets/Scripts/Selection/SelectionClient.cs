﻿using UnityEngine;

namespace Clay.Selection
{
    public class SelectionClient : MonoBehaviour
    {
        [SerializeField] private SelectionManager selectionManagerSo;
        [SerializeField] private Renderer[]       renderersForIndication;
        [SerializeField] private Material         indicationMaterial;

        public void TakeSelection()
        {
            selectionManagerSo.TakeSelection(SetToNonSelected, this.transform);
            SetToSelected();
        }

        void SetToNonSelected()
        {
            if (this == null || this.gameObject == null)
            {
                return;
            }
            
            foreach (var rendererForIndication in renderersForIndication)
            {
                var materials = rendererForIndication.materials;
                if (materials.Length <= 1)
                {
                    return;
                }

                var newMaterials = new Material[1];
                newMaterials[0] = materials[0];
                rendererForIndication.materials = newMaterials;
            }
        }

        void SetToSelected()
        {
            foreach (var rendererForIndication in renderersForIndication)
            {
                var materials = new Material[2];
                materials[0] = rendererForIndication.materials[0];
                if (rendererForIndication.materials.Length < 2)
                {
                    materials[1] = indicationMaterial;
                }

                rendererForIndication.materials = materials;
            }
        }
    }
}
