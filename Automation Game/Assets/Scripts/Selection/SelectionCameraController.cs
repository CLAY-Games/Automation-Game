﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clay.Selection
{
    public class SelectionCameraController : MonoBehaviour
    {
        [SerializeField] private SelectionManager selectionManager;
        [SerializeField] private float rotationSpeed;

        private void LateUpdate()
        {
            if(selectionManager.SelectedObject == null)
            {
                return;
            }

            transform.position = selectionManager.SelectedObject.position;
            transform.Rotate(new Vector3(0, rotationSpeed * Time.deltaTime, 0));
        }
    }
}
