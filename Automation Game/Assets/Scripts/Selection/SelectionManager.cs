﻿using Clay.Input;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Clay.Selection
{
    [CreateAssetMenu(fileName = "SelectionManager.asset", menuName = "ScriptableObjects/SelectionManager")]
    public class SelectionManager : ScriptableObject
    {
        private UnityAction currentOnLoseSelectionCallback;

        [SerializeField] private InputManager inputManagerSo;
        public                   Transform    SelectedObject { get; private set; }

        private void OnEnable()
        {
            inputManagerSo.DeselectAll += OnDeselectAll;
        }

        private void OnDisable()
        {
            inputManagerSo.DeselectAll -= OnDeselectAll;
        }

        private void OnDeselectAll(InputAction.CallbackContext _)
        {
            currentOnLoseSelectionCallback?.Invoke();
            currentOnLoseSelectionCallback = null;
        }

        public void TakeSelection(UnityAction onLoseSelectionCallback, Transform selectedTransform)
        {
            currentOnLoseSelectionCallback?.Invoke();
            currentOnLoseSelectionCallback = onLoseSelectionCallback;
            SelectedObject = selectedTransform;
        }
    }
}
