﻿using System.Collections;

using UnityEngine;

namespace Clay
{
    public class SpiderWalkAnimator : MonoBehaviour
    {
        [SerializeField] private Transform[] inverseKinematicTargets;

        [SerializeField] private float stepSize   = 0.5f;
        [SerializeField] private float stepHeight = 0.4f;
        [SerializeField] private float stepSpeed  = 4;

        private Vector3[] defaultLegPositions;
        private Vector3[] lastLegPositions;
        private bool[]    legMoving;
        private Vector3   lastBodyPosition;

        private void Awake()
        {
            lastLegPositions = new Vector3[inverseKinematicTargets.Length];
            legMoving = new bool[inverseKinematicTargets.Length];
            defaultLegPositions = new Vector3[inverseKinematicTargets.Length];

            lastBodyPosition = transform.position;

            for (int i = 0; i < inverseKinematicTargets.Length; ++i)
            {
                defaultLegPositions[i] = inverseKinematicTargets[i].localPosition;
                lastLegPositions[i] = inverseKinematicTargets[i].position;
                legMoving[i] = false;
            }
        }

        private void FixedUpdate()
        {
            int indexToMove = GetFurthestLegIndex();

            if (indexToMove != -1 && !legMoving[indexToMove] && CanLiftLeg())
            {
                legMoving[indexToMove] = true;
                StartCoroutine(StepRoutine(indexToMove));
            }

            StickTargetsToWorldPosition();

            lastBodyPosition = transform.position;
        }

        bool CanLiftLeg()
        {
            int groundedLegCount = 0;

            for (int i = 0; i < inverseKinematicTargets.Length; ++i)
            {
                if(legMoving[i] == false)
                {
                    groundedLegCount++;
                }
            }

            if(groundedLegCount > 2)
            {
                return true;
            }

            return false;
        }

        IEnumerator StepRoutine(int index)
        {
            float stepTime = 1f / stepSpeed;
            float remainingStepTime = stepTime;

            Vector3 startPoint = inverseKinematicTargets[index].position;
            Vector3 followThrough = (transform.position - lastBodyPosition).normalized * stepSize;

            while (remainingStepTime > 0f)
            {
                float progress = 1f - (remainingStepTime / stepTime);
                inverseKinematicTargets[index].position = Helper.Utils.GetPointOnBezierCurve(startPoint, followThrough + transform.TransformPoint(defaultLegPositions[index]), stepHeight, progress);
                remainingStepTime -= Time.deltaTime;
                yield return null;
            }

            legMoving[index] = false;
            lastLegPositions[index] = inverseKinematicTargets[index].position;

            yield break;
        }

        int GetFurthestLegIndex()
        {
            int indexToMove = -1;
            float maxDistance = stepSize;

            for (int i = 0; i < inverseKinematicTargets.Length; ++i)
            {
                float defaultDistance = Vector3.Distance(transform.TransformPoint(defaultLegPositions[i]), inverseKinematicTargets[i].position);

                if (defaultDistance > maxDistance)
                {
                    maxDistance = defaultDistance;
                    indexToMove = i;
                }
            }

            return indexToMove;
        }

        void StickTargetsToWorldPosition()
        {
            for (int i = 0; i < inverseKinematicTargets.Length; ++i)
            {
                if (legMoving[i] == false)
                {
                    inverseKinematicTargets[i].position = lastLegPositions[i];
                }
            }
        }

        private void OnDrawGizmosSelected()
        {
            if(defaultLegPositions == null)
            {
                return;
            }

            for (int i = 0; i < defaultLegPositions.Length; i++)
            {
                UnityEditor.Handles.DrawWireDisc(transform.TransformPoint(defaultLegPositions[i]), transform.up, stepSize);
            }
        }
    }
}
