using Clay.Input;

using UnityEngine;
using UnityEngine.InputSystem;

namespace Clay
{
    public class ClickHandler : MonoBehaviour
    {
        [SerializeField] private InputManager inputManager;

        private Camera mainCamera;

        private void Awake()
        {
            mainCamera = Camera.main;
            inputManager.OnClickPerformed += OnClickPerformed;
        }

        private void OnClickPerformed(InputAction.CallbackContext ctx)
        {
            Vector2 mousePos = inputManager.GetMousePosition();
            var ray = mainCamera.ScreenPointToRay(mousePos);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                Debug.DrawLine(mainCamera.transform.position, hit.point, Color.red, 1f);
                var clickable = hit.transform.GetComponent<IClickable>();
                if (clickable != null)
                {
                    clickable.Click();
                }
            }
        }
    }
}
