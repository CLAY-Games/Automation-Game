using System;
using System.Collections.Generic;

using Clay.Input;
using Clay.UI.PlayerBase;

using UnityEngine;

namespace Clay.Base
{
    [RequireComponent(typeof(IPlayerBaseUiConnector))]
    public class PlayerBaseUiController : MonoBehaviour, IPlayerBaseUiController
    {
        public event Action OnBuildNewUnit;
        public int          CaspiumCapacity  { set => ui.CaspiumCapacity = value; }
        public int          CaspiumAmount    { set => ui.CaspiumAmount = value; }
        public int          BlotriumCapacity { set => ui.BlotriumCapacity = value; }
        public int          BlotriumAmount   { set => ui.BlotriumAmount = value; }
        public int          UnitConst        { set => ui.UnitPrice = value; }

        [SerializeField] private InputManager inputManager;

        private IPlayerBaseUiConnector ui;

        private float timeUntilNextUnit;
        private float totalTimeForNextUnit;

        // float indicates the time it takes for the unit to be built
        private readonly Queue<float> unitQueue = new();


        public void AddUnitToQueue(float spawnTime)
        {
            unitQueue.Enqueue(spawnTime);

            if (unitQueue.Count == 0)
            {
                StartNewUnit();
            }
            else // update ui.NrUnitsInProduction in both cases
            {
                ui.NrUnitsInProduction = unitQueue.Count + 1;
            }
        }

        public void EnableUi() => ui.EnableUi();

        private void Awake()
        {
            ui = GetComponent<IPlayerBaseUiConnector>();
        }

        private void Start()
        {
            inputManager.DeselectAll += _ => ui.DisableUi();

            ui.OnBuildNewUnit += () => OnBuildNewUnit?.Invoke();
            InitializeUi();
            ui.DisableUi();
        }


        private void Update()
        {
            timeUntilNextUnit -= Time.deltaTime;
            if (timeUntilNextUnit <= 0)
            {
                HandleUnitFinished();
                return;
            }

            ui.TimeUntilNextUnit = timeUntilNextUnit;
        }

        private void HandleUnitFinished()
        {
            if (unitQueue.Count > 0)
            {
                StartNewUnit();
                return;
            }

            InitializeUi();
        }

        private void InitializeUi()
        {
            ui.TimeUntilNextUnit = 0;
            ui.TotalTimeForNextUnit = 0.1f;
            ui.NrUnitsInProduction = 0;
        }


        private void StartNewUnit()
        {
            timeUntilNextUnit = unitQueue.Dequeue();
            ui.NrUnitsInProduction = unitQueue.Count + 1; // +1 to account for the unit currently being created
            ui.TimeUntilNextUnit = timeUntilNextUnit;
            ui.TotalTimeForNextUnit = timeUntilNextUnit;
        }
    }
}
