﻿using Clay.Colony;
using Clay.Helper;
using Clay.Selection;

using UnityEngine;

namespace Clay.Base
{
    [RequireComponent(typeof(UnitSpawner), typeof(IPlayerBaseUiController), typeof(SelectionClient))]
    public class PlayerBase : MonoBehaviour, IClickable
    {
        [SerializeField] private ColonyResourceStorage resourceStorageSo;
        [SerializeField] private Colony.Colony         playerColony;

        private IPlayerBaseUiController uiController;
        private UnitSpawner             unitSpawner;
        private SelectionClient         selectionClient;

        private void Start()
        {
            SetUpCallbacks();
            InitializeUi();
        }

        private void SetUpCallbacks()
        {
            uiController.OnBuildNewUnit += HandleNewUnitRequest;

            resourceStorageSo.OnCaspiumCapacityChanged += newCapacity => uiController.CaspiumCapacity = newCapacity;
            resourceStorageSo.OnCaspiumAmountChanged += newAmount => uiController.CaspiumAmount = newAmount;

            resourceStorageSo.OnBlotriumCapacityChanged += newCapacity => uiController.BlotriumCapacity = newCapacity;
            resourceStorageSo.OnBlotriumAmountChanged += newAmount => uiController.BlotriumAmount = newAmount;
        }

        private void HandleNewUnitRequest()
        {
            if (!playerColony.RequestUnit(unitSpawner.UnitResourceCost))
            {
                // TODO player feedback
                return;
            }

            uiController.AddUnitToQueue(unitSpawner.UnitSpawnTime);
            unitSpawner.AddUnitToSpawnQueue();
        }

        private void InitializeUi()
        {
            uiController.CaspiumCapacity = resourceStorageSo.CaspiumCapacity;
            uiController.CaspiumAmount = resourceStorageSo.CaspiumAmount;

            uiController.BlotriumCapacity = resourceStorageSo.BlotriumCapacity;
            uiController.BlotriumAmount = resourceStorageSo.BlotriumAmount;

            uiController.UnitConst = unitSpawner.UnitResourceCost;
        }

        public void Click()
        {
            selectionClient.TakeSelection();
            uiController.EnableUi();
        }

        private void Awake()
        {
            uiController = GetComponent<PlayerBaseUiController>();
            selectionClient = GetComponent<SelectionClient>();
            unitSpawner = GetComponent<UnitSpawner>();
        }
    }
}
