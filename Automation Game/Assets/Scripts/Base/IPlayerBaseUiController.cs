﻿using System;

namespace Clay.Base
{
    public interface IPlayerBaseUiController
    {
        public event Action OnBuildNewUnit;

        public int CaspiumCapacity  { set; }
        public int CaspiumAmount    { set; }
        public int BlotriumCapacity { set; }
        public int BlotriumAmount   { set; }
    
        public int UnitConst { set; }

        public void AddUnitToQueue(float spawnTime);

        public void EnableUi();
    }
}
