using System;
using System.Collections.Generic;
using System.Linq;

using Clay.Colony;
using Clay.Helper;

using UnityEngine;

namespace Clay.Base.Enemy
{
    public class EnemyAi : MonoBehaviour
    {
        [SerializeField] private ColonyResourceStorage resourceStorage;
        [SerializeField] private Colony.Colony         colony;
        [SerializeField] private float                 checkInterval = 0.5f;
        [SerializeField] private UnitSpawner           unitSpawner;

        private void Start()
        {
            InvokeRepeating(nameof(AiTickImpl), 0, checkInterval);
        }

        private void AiTickImpl()
        {
            TrySpawnUnit();
            BalanceUnitsPreferredResource();
        }

        private void BalanceUnitsPreferredResource()
        {
            var (caspiumCount, blotriumCount) = CountPreferredResources(colony.units);

            if (Mathf.Abs(caspiumCount - blotriumCount) <= 1)
            {
                return;
            }

            ResourceType typeToChange = caspiumCount < blotriumCount ? ResourceType.Blotrium : ResourceType.Caspium;

            Unit.Unit unit = colony.units.FirstOrDefault(unit => unit.PreferredResourceType == typeToChange);
            if (unit == null)
            {
                return;
            }

            ResourceType newTypeToUse = typeToChange == ResourceType.Blotrium ? ResourceType.Caspium : ResourceType.Blotrium;
            unit.PreferredResourceType = newTypeToUse;
        }

        private (int caspiumCount, int blotriumCount) CountPreferredResources(List<Unit.Unit> units)
        {
            int caspiumCount = 0;
            int blotriumCount = 0;
            foreach (Unit.Unit unit in units)
            {
                switch (unit.PreferredResourceType)
                {
                    case ResourceType.Caspium:
                        caspiumCount++;
                        break;
                    case ResourceType.Blotrium:
                        blotriumCount++;
                        break;
                    default:
                        Debug.LogError("Invalid ResourceType");
                        break;
                }
            }

            return (caspiumCount, blotriumCount);
        }

        private void TrySpawnUnit()
        {
            if (colony.RequestUnit(unitSpawner.UnitResourceCost))
            {
                unitSpawner.AddUnitToSpawnQueue();
            }
        }
    }
}
