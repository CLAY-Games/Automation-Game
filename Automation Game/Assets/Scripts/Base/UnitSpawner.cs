﻿using Clay.Helper;

using UnityEditor;

using UnityEngine;

namespace Clay.Base
{
    public class UnitSpawner : MonoBehaviour
    {
        public float UnitSpawnTime    { get; private set; } = 2f;
        public int   UnitResourceCost { get; private set; } = 200;

        [SerializeField] private GameObject unitPrefab;
        [SerializeField] private Transform  spawnPos;

        private int queueSize = 0;


        public void AddUnitToSpawnQueue()
        {
            if (queueSize == 0)
            {
                Invoke(nameof(OnSpawnUnitTimeOver), UnitSpawnTime);
            }
            
            queueSize++;
        }

        public void SpawnUnitInstantly()
        {
            SpawnUnitImpl();
        }


        private void OnSpawnUnitTimeOver()
        {
            queueSize--;
            SpawnUnitImpl();

            if (queueSize > 0)
            {
                Invoke(nameof(OnSpawnUnitTimeOver), UnitSpawnTime);
            }
        }

        private void SpawnUnitImpl()
        {
            Instantiate(unitPrefab, spawnPos.position, spawnPos.rotation);
        }
        

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            var allResources = GameObject.FindGameObjectsWithTag(Tags.Resource);

            foreach (var resource in allResources)
            {
                var directionToResource = resource.transform.position - transform.position;
                Debug.DrawLine(transform.position, resource.transform.position, Color.yellow);

                var distance = (directionToResource).magnitude;
                Handles.Label(transform.position + directionToResource.normalized * 5, $"Distance: {distance:#.00}");
            }
        }
#endif
    }
}
