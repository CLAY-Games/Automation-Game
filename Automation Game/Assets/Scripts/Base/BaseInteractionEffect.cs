﻿using UnityEngine;

namespace Clay.Base
{
    [RequireComponent(typeof(ColonyResourceStorageAccess))]
    public class BaseInteractionEffect : MonoBehaviour
    {
        [SerializeField] private ParticleSystem OnInteractionEffect;

        private ColonyResourceStorageAccess resourceStorage;

        private void Awake()
        {
            resourceStorage = GetComponent<ColonyResourceStorageAccess>();
        }

        private void Start()
        {
            resourceStorage.OnInteraction += PlayInteractionEffect;
        }

        void PlayInteractionEffect()
        {
            OnInteractionEffect.Play();
        }
    }
}
