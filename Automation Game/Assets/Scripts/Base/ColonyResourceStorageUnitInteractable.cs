using UnityEngine;

namespace Clay.Base
{
    public class ColonyResourceStorageUnitInteractable : MonoBehaviour, IUnitInteractable
    {
        [SerializeField] ColonyResourceStorageAccess resourceStorage;

        public GameObject GetRoot()
        {
            return resourceStorage.gameObject;
        }
    }
}
