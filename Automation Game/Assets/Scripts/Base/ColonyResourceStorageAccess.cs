using Clay.Colony;
using Clay.Helper;

using UnityEngine;
using UnityEngine.Events;

namespace Clay.Base
{
    public class ColonyResourceStorageAccess : MonoBehaviour
    {
        public event UnityAction OnInteraction;

        [SerializeField] private ColonyResourceStorage resourceStorage;

        /// <returns>The amount of resources that could be successfully withdrawn</returns>
        public int RequestResources(ResourceType resourceType, int amount)
        {
            OnInteraction?.Invoke();
        
            int currentAmount = GetResourceAmount(resourceType);
            int newFill = currentAmount - amount;
            if (newFill >= 0)
            {
                SetResourceAmount(resourceType, newFill);
                return amount;
            }

            int remainingFill = currentAmount;
            SetResourceAmount(resourceType, 0);

            return remainingFill;
        }

        private void SetResourceAmount(ResourceType type, int amount)
        {
            if(type == ResourceType.Blotrium)
            {
                resourceStorage.BlotriumAmount = amount;
            }
            else
            {
                resourceStorage.CaspiumAmount = amount;
            }
        }

        private int GetResourceCapacity(ResourceType type)
        {
            return type == ResourceType.Blotrium ? resourceStorage.BlotriumCapacity : resourceStorage.CaspiumCapacity;
        }
        private int GetResourceAmount(ResourceType type)
        {
            return type == ResourceType.Blotrium ? resourceStorage.BlotriumAmount : resourceStorage.CaspiumAmount;
        }

        /// <returns>The amount of resources that were successfully stored</returns>
        public int StoreResources(ResourceType resourceType, int amountToStore)
        {
            OnInteraction?.Invoke();
        
            int currentAmount = GetResourceAmount(resourceType);
            int capacity = GetResourceCapacity(resourceType);
            if (currentAmount + amountToStore > capacity)
            {
                int amountStored = capacity - currentAmount;
                SetResourceAmount(resourceType, capacity);
                return amountStored;
            }

            SetResourceAmount(resourceType, currentAmount + amountToStore);
            return amountToStore;
        }
    }
}
