﻿using System;

using UnityEngine;
using UnityEngine.AI;

namespace Clay.Unit
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class UnitMovement : MonoBehaviour
    {
        public bool         DestinationWasReached { get; private set; } = false;
        public event Action OnDestinationReached;

        private NavMeshAgent navMeshAgent;

        private float checkDestinationStatePollInterval = 0.1f;


        private void Awake()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
        }


        public void SetDestination(Vector3 destination)
        {
            DestinationWasReached = false;
            navMeshAgent.destination = destination;
        }


        void Start()
        {
            InvokeRepeating(nameof(CheckDestinationReachedState), 0, checkDestinationStatePollInterval);
        }


        // TODO find a better name
        void CheckDestinationReachedState()
        {
            if (DestinationWasReached)
            {
                return;
            }

            if (WasDestinationReached())
            {
                DestinationWasReached = true;
                OnDestinationReached?.Invoke();
            }
        }


        /// <returns>True when destination was reached, or was failed to reach</returns>
        bool WasDestinationReached()
        {
            // Pathfinding calculation not yet finished
            if (navMeshAgent.pathPending)
            {
                return false;
            }

            // too far away from target to begin decelerating
            if (navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance)
            {
                return false;
            }

            // Destination is unreachable
            if (!navMeshAgent.hasPath)
            {
                return true;
            }

            return true;
        }


        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            if (navMeshAgent != null)
            {
                Gizmos.DrawWireSphere(navMeshAgent.destination, 0.5f);
            }
        }
    }
}
