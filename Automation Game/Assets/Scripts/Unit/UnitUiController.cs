﻿using System;

using Clay.Helper;
using Clay.Input;
using Clay.UI.Unit;

using UnityEngine;
using UnityEngine.Events;

namespace Clay.Unit
{
    [RequireComponent(typeof(UnitCombat))]
    [RequireComponent(typeof(UnitHarvesting))]
    [RequireComponent(typeof(Unit))]
    [RequireComponent(typeof(IUnitUiConnector))]
    public class UnitUiController : MonoBehaviour
    {
        [SerializeField] private InputManager inputManager;
        [SerializeField] private HelperSo     helperSo;

        [SerializeField] private bool interactable;

        private Unit             unit;
        private UnitCombat       unitCombat;
        private UnitHarvesting   unitHarvesting;
        private IUnitUiConnector uiConnector;

        private ResourceType preferredResouceType;

        public event UnityAction<ResourceType> OnPreferredResourceTypeChanged;

        public void EnableUi() => uiConnector.EnableUi();

        private void Awake()
        {
            unit = GetComponent<Unit>();
            uiConnector = GetComponent<IUnitUiConnector>();
            unitCombat = GetComponent<UnitCombat>();
            unitHarvesting = GetComponent<UnitHarvesting>();
        }

        private void Start()
        {
            RegisterCallbacks();
            InitializeUi();
            uiConnector.DisableUi();
        }

        private void RegisterCallbacks()
        {
            uiConnector.OnPreferredResourceTypeClicked += OnPreferredResourceTypeClicked;
            unit.OnPreferredResourceTypeChanged += rt =>
            {
                preferredResouceType = rt;
                SetPreferredResourceUi();
            };
            
            unitCombat.OnHealthChanged += newHp => uiConnector.Hp = newHp;
            unitHarvesting.OnDepositingDone += OnResourcesAmountChanged;
            unitHarvesting.OnHarvestingDone += OnResourcesAmountChanged;

            inputManager.DeselectAll += _ => uiConnector.DisableUi();
        }

        private void OnPreferredResourceTypeClicked()
        {
            if (!interactable)
            {
                return;
            }

            preferredResouceType = preferredResouceType == ResourceType.Blotrium ? ResourceType.Caspium : ResourceType.Blotrium;

            SetPreferredResourceUi();

            OnPreferredResourceTypeChanged?.Invoke(preferredResouceType);
        }

        private void SetPreferredResourceUi()
        {
            uiConnector.PreferredResourceIcon = helperSo.GetResourceIcon(preferredResouceType);
            uiConnector.PreferredResourceType = preferredResouceType.ToString();
            uiConnector.PreferredResourceTypeColor = Utils.ResourceTypeToColor(preferredResouceType);
        }

        private void OnResourcesAmountChanged()
        {
            uiConnector.ResourceColor = GetResourceBarColor();
            uiConnector.ResourceTitle = unitHarvesting.CurrentlyCarriedResourceType.ToString();
            uiConnector.ResourcesAmount = unitHarvesting.CurrentlyCarrying;
        }

        private void InitializeUi()
        {
            preferredResouceType = unit.PreferredResourceType;
            SetPreferredResourceUi();
            OnResourcesAmountChanged();
            uiConnector.Damage = unitCombat.Damage;
            uiConnector.Hp = unitCombat.CurrentHealth;
            uiConnector.MaxHp = unitCombat.MaxHealth;
            uiConnector.Range = unitCombat.CombatRadius;
            uiConnector.ResourcesCapacity = unitHarvesting.Capacity;
        }

        private Color GetResourceBarColor()
        {
            return unitHarvesting.CurrentlyCarriedResourceType switch
            {
                ResourceType.Caspium  => Colors.Caspium,
                ResourceType.Blotrium => Colors.Blotrium,
                var _                 => Color.grey
            };
        }
    }
}
