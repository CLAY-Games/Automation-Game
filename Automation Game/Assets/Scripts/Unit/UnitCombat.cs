﻿using UnityEngine;
using UnityEngine.Events;

namespace Clay.Unit
{
    public class UnitCombat : MonoBehaviour
    {
        [Header("Stats")]
        public int MaxHealth = 100;
        public int   Damage       = 8;
        public float CombatRadius = 10f;

        [Header("Enemy Detection Parameters")]
        [SerializeField] private LayerMask enemyLayerMask;
        [SerializeField] private float checkForEnemiesInterval = 0.5f;

        [Header("References")]
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private ParticleSystem onHitEffect;

        // Parameter indicates the new health amount
        public event UnityAction<int> OnHealthChanged; 

        public int CurrentHealth { private set; get; }

    
        private void Awake()
        {
            CurrentHealth = MaxHealth;
        }

        private void Start()
        {
            InvokeRepeating(nameof(CheckForNearestEnemies), checkForEnemiesInterval, checkForEnemiesInterval);
        }

        void CheckForNearestEnemies()
        {
            Collider[] enemies = Physics.OverlapSphere(transform.position, CombatRadius, enemyLayerMask);

            float distToNearest = float.PositiveInfinity;
            GameObject nearestEnemy = null;

            foreach (var currentEnemy in enemies)
            {
                float distToCurrent = Vector3.Distance(currentEnemy.transform.position, this.transform.position);
                if (distToCurrent < distToNearest)
                {
                    distToNearest = distToCurrent;
                    nearestEnemy = currentEnemy.gameObject;
                }
            }

            if (nearestEnemy == null)
            {
                return;
            }

            Attack(nearestEnemy);
        }

        internal void TakeDamage(int damage)
        {
            this.CurrentHealth -= damage;
            OnHealthChanged?.Invoke(this.CurrentHealth);
            onHitEffect.Play();
            if (this.CurrentHealth <= 0)
            {
                Destroy(this.gameObject);
            }
        }

        private void Attack(GameObject nearestEnemy)
        {
            var bullet = Instantiate(bulletPrefab, transform.position + transform.forward, transform.rotation);
            var bulletComponent = bullet.GetComponent<Bullet>();

            bulletComponent.Shoot(nearestEnemy.GetComponent<UnitCombat>(), Damage);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, CombatRadius);
        }
    }
}
