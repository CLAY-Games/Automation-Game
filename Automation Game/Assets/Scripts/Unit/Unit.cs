﻿using System;
using System.Collections.Generic;
using System.Linq;

using Clay.Helper;
using Clay.Selection;

using UnityEngine;
using UnityEngine.Events;

namespace Clay.Unit
{
    [RequireComponent(typeof(UnitUiController))]
    [RequireComponent(typeof(UnitHarvesting))]
    [RequireComponent(typeof(UnitMovement))]
    [RequireComponent(typeof(SelectionClient))]
    public class Unit : MonoBehaviour, IClickable
    {
        [TagSelector]
        public string TeamBaseTag = "";

        public event UnityAction<ResourceType> OnPreferredResourceTypeChanged;

        [SerializeField] private Colony.Colony colony;
        [SerializeField] private ResourceType  preferredResourceType;

        private UnitHarvesting   harvesting;
        private UnitMovement     movement;
        private UnitUiController uiController;
        private SelectionClient  selectionClient;

        private State state = State.Harvesting;

        public ResourceType PreferredResourceType
        {
            get => preferredResourceType;
            set
            {
                preferredResourceType = value;
                OnPreferredResourceTypeChanged?.Invoke(value);
                RecalculateDestinationIfHarvesting();
            }
        }

        private void RecalculateDestinationIfHarvesting()
        {
            if (state != State.Harvesting)
            {
                return;
            }

            BeginHarvesting();
        }

        enum State
        {
            Harvesting,
            Depositing
        }


        private void Awake()
        {
            harvesting = GetComponent<UnitHarvesting>();
            movement = GetComponent<UnitMovement>();
            uiController = GetComponent<UnitUiController>();
            selectionClient = GetComponent<SelectionClient>();
        }


        private void Start()
        {
            RegisterCallbacks();
            DoActionBasedOnState();
            colony.RegisterUnit(this);
        }

        private void RegisterCallbacks()
        {
            uiController.OnPreferredResourceTypeChanged += rt => PreferredResourceType = rt;
            harvesting.OnHarvestingDone += OnHarvestingDone;
            harvesting.OnDepositingDone += OnDepositingDone;

            movement.OnDestinationReached += DoActionBasedOnState;
        }


        private void OnDepositingDone()
        {
            state = State.Harvesting;
            DoActionBasedOnState();
        }


        private void OnHarvestingDone()
        {
            state = State.Depositing;
            DoActionBasedOnState();
        }


        void DoActionBasedOnState()
        {
            switch (state)
            {
                case State.Harvesting:
                    BeginHarvesting();
                    break;
                case State.Depositing:
                    BeginDepositing();
                    break;
            }
        }


        void BeginHarvesting()
        {
            harvesting.ShouldHarvest = true;
            harvesting.ShouldDeposit = false;

            var resourceStorages = GetResourceStorages();

            bool allTypesAreNonPreferred = resourceStorages.All(storage => storage.Type != preferredResourceType);
            var nearest = GetNearestResourceStorage(resourceStorages, allTypesAreNonPreferred ? null : preferredResourceType);

            if (nearest != null)
            {
                movement.SetDestination(nearest.transform.position);
            }
        }

        private static List<ResourceStorage> GetResourceStorages()
        {
            List<ResourceStorage> resourceStorages = new List<ResourceStorage>();
            var allResources = GameObject.FindGameObjectsWithTag(Tags.Resource);
            foreach (GameObject resource in allResources)
            {
                ResourceStorage resourceStorage = resource.GetComponent<ResourceStorage>();
                if (resourceStorage == null)
                {
                    Debug.LogError("FindGameObjectsWithTag(Tags.Resource) returned an Object which does not have a ResourceStorage component.");
                }
                else
                {
                    resourceStorages.Add(resourceStorage);
                }
            }

            return resourceStorages;
        }

        GameObject GetNearestResourceStorage(List<ResourceStorage> resourceStorages, ResourceType? resourceType)
        {
            GameObject nearest = null;
            float minDist = float.PositiveInfinity;

            foreach (var current in resourceStorages)
            {
                if (resourceType.HasValue && resourceType != current.Type)
                {
                    continue;
                }

                float dist = (current.transform.position - this.transform.position).magnitude;
                if (dist < minDist)
                {
                    minDist = dist;
                    nearest = current.gameObject;
                }
            }

            return nearest;
        }


        void BeginDepositing()
        {
            harvesting.ShouldDeposit = true;
            harvesting.ShouldHarvest = false;

            var allFriendlyBases = GameObject.FindGameObjectsWithTag(TeamBaseTag);
            var nearestBase = GetNearest(allFriendlyBases);
            movement.SetDestination(nearestBase.transform.position);
        }


        GameObject GetNearest(GameObject[] objects)
        {
            GameObject nearest = null;
            float minDist = float.PositiveInfinity;

            foreach (var current in objects)
            {
                float dist = (current.transform.position - this.transform.position).magnitude;
                if (dist < minDist)
                {
                    minDist = dist;
                    nearest = current;
                }
            }

            return nearest;
        }

        public void Click()
        {
            uiController.EnableUi();
            selectionClient.TakeSelection();
        }

        private void OnDestroy()
        {
            colony.OnUnitDestroyed(this);
        }
    }
}
