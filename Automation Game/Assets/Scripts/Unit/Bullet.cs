﻿using System.Collections;
using UnityEngine;

namespace Clay.Unit
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private ParticleSystem hitEffect;

        [Header("Stats")]
        [SerializeField] private float speed = 1f;
        [SerializeField] private float travelHeight = 10f;


        private UnitCombat target;
        private int        damage;

        private Vector3 initialPosition;

        public void Shoot(UnitCombat target, int damage)
        {
            this.target = target;
            initialPosition = transform.position;
            this.damage = damage;

            StartCoroutine(TravelToTarget());
        }

        private IEnumerator TravelToTarget()
        {
            float progress = 0f;

            while(progress < 1f)
            {
                if (target == null)
                {
                    DetachThrusterParticles();
                    Destroy(this.gameObject);
                    yield break;
                }

                transform.position = Helper.Utils.GetPointOnBezierCurve(initialPosition, target.transform.position, travelHeight, progress);
                transform.LookAt(target.transform.position);

                progress += Time.deltaTime * speed;
                yield return null;
            }

            DamageTarget();
            yield break;
        }

        private void DetachThrusterParticles()
        {
            ParticleSystem ps = GetComponentInChildren<ParticleSystem>();
            ps.transform.parent = null;

            ps.Stop();
        }

        private void DamageTarget()
        {
            DetachThrusterParticles();
            Instantiate(hitEffect, transform.position, Quaternion.identity);

            target.TakeDamage(damage);
            Destroy(this.gameObject);
        }
    }
}
