﻿using System;

using Clay.Base;
using Clay.Helper;

using UnityEditor;

using UnityEngine;

namespace Clay.Unit
{
    [RequireComponent(typeof(Unit))]
    public class UnitHarvesting : MonoBehaviour
    {
        public bool ShouldHarvest { get; set; } = false;
        public bool ShouldDeposit { get; set; } = false;

        public int Capacity          { get => capacity;          private set => capacity = value; }
        public int CurrentlyCarrying { get => currentlyCarrying; private set => currentlyCarrying = value; }

        public ResourceType CurrentlyCarriedResourceType { get; private set; }

        public event Action OnHarvestingDone;
        public event Action OnDepositingDone;

        private                  string teamTag;
        [SerializeField] private int    capacity;
        [SerializeField] private int    currentlyCarrying;


        private void Awake()
        {
            teamTag = GetComponent<Unit>().TeamBaseTag;
        }


        private void OnTriggerEnter(Collider other)
        {
            var interactableGameObject = other.GetComponent<IUnitInteractable>()?.GetRoot();
            if (interactableGameObject == null)
            {
                return;
            }

            if (interactableGameObject.CompareTag(Tags.Resource))
            {
                HandleEnterResourceTrigger(interactableGameObject);
                return;
            }

            if (interactableGameObject.CompareTag(teamTag))
            {
                HandleEnterTeamTagCollider(interactableGameObject);
                return;
            }
        }


        private void HandleEnterResourceTrigger(GameObject interactableObject)
        {
            if (!ShouldHarvest)
            {
                return;
            }

            var resource = interactableObject.GetComponent<ResourceStorage>();
            if (resource == null)
            {
                Debug.LogError("Object tagged as resource does not have a Resource component");
                return;
            }

            Harvest(resource);
        }

        // TODO harvesting and depositing should be generalized into some kind of transaction system

        private void Harvest(ResourceStorage resource)
        {
            int amountToRequest = Capacity - currentlyCarrying;
            var (resourceType, receivedAmount) = resource.RequestResources(amountToRequest);

            // avoid switching resource types
            if (currentlyCarrying > 0 && resourceType != this.CurrentlyCarriedResourceType)
            {
                OnHarvestingDone?.Invoke();
                return;
            }

            CurrentlyCarriedResourceType = resourceType;
            currentlyCarrying += receivedAmount;
            OnHarvestingDone?.Invoke();

            // TODO some kind of harvesting animation
            // TODO harvesting time
            // TODO a more elaborate harvesting system in general
        }


        private void HandleEnterTeamTagCollider(GameObject interactableObject)
        {
            if (!ShouldDeposit)
            {
                return;
            }

            var friendlyResourceStorage = interactableObject.GetComponent<ColonyResourceStorageAccess>();
            if (friendlyResourceStorage == null)
            {
                Debug.LogWarning("Entered collider of friendly team, but it is not a ResourceStorage");
                return;
            }

            Deposit(friendlyResourceStorage);
        }


        private void Deposit(ColonyResourceStorageAccess resourceStorage)
        {
            int actuallyStored = resourceStorage.StoreResources(CurrentlyCarriedResourceType, currentlyCarrying);

            currentlyCarrying -= actuallyStored;

            OnDepositingDone?.Invoke();

            // TODO some kind of depositing animation
            // TODO depositing time
            // TODO a more elaborate depositing system in general
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Handles.Label(transform.position + new Vector3(0, 1, 0), currentlyCarrying.ToString());
        }
#endif
    }
}
