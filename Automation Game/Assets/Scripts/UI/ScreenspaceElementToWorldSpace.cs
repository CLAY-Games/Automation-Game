﻿using UnityEngine;

namespace Clay.UI
{
    public class ScreenspaceElementToWorldSpace : MonoBehaviour
    {
        private Camera cam;

        [SerializeField] Transform target;


        [Range(-0.5f, 0.5f)]
        [SerializeField]
        float offsetX = 0f;

        [Range(-0.5f, 0.5f)]
        [SerializeField]
        float offsetY = 0f;

        public void Awake()
        {
            cam = Camera.main;
        }

        void Update()
        {
            Vector3 screenSpacePosition = cam.WorldToScreenPoint(target.position);

            screenSpacePosition += new Vector3(offsetX * Screen.width, offsetY * Screen.height);

            //TODO - Clamp to stay on screen at all times.

            if (transform.position != screenSpacePosition)
            {
                transform.position = screenSpacePosition;
            }
        }
    }
}
