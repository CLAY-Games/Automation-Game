using System.Collections.Generic;

using Clay.Input;

using UnityEngine;
using UnityEngine.EventSystems;

namespace Clay
{
    public class CameraRigController : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private Transform transformToGenerateBoundsFrom;

        [SerializeField] private Vector3     boundsPadding;
        [SerializeField] private EventSystem eventSystem;

        [Header("Stats")]
        public float panSpeed = 20f;

        public float panBorderThickness = 10f;
        public float smoothingSpeed     = 16;

        public Vector2 zoomBounds  = new Vector2(2, 200);
        public float   scrollSpeed = 5f;

        public float rotationMultiplier = 20f;

        public bool enablePanByMouseAtBorder = false;


        private Vector3 targetPosition;
        private Vector3 leftMouseButtonClickWorldPosition;

        private Vector2 rightMouseButtonClickPosition;

        private float currentZoomValue          = 60;
        private bool  leftMouseButtonIsPressed  = false;
        private bool  rightMouseButtonIsPressed = false;

        private Camera mainCamera;
        private Bounds camBounds;

        [SerializeField] private InputManager inputManager;

        private readonly Vector3 rigToCamDirection = new Vector3(0, 1.5f, -1);


        private void Awake()
        {
            mainCamera = Camera.main;

            SetCameraBounds();
            InitializeCamera();
        }


        private void Start()
        {
            this.inputManager.OnClickStarted += ctx =>
            {
                leftMouseButtonIsPressed = true;
                leftMouseButtonClickWorldPosition = GetMouseInWorldPosition();
            };

            this.inputManager.OnClickCanceled += ctx =>
            {
                leftMouseButtonIsPressed = false;
            };


            this.inputManager.OnClickRmbStarted += ctx =>
            {
                rightMouseButtonIsPressed = true;
                rightMouseButtonClickPosition = this.inputManager.GetMousePosition();
            };

            this.inputManager.OnClickRmbCanceled += ctx =>
            {
                rightMouseButtonIsPressed = false;
            };
        }


        void InitializeCamera()
        {
            currentZoomValue = (zoomBounds.x + zoomBounds.y)       / 2;
            mainCamera.transform.localPosition = rigToCamDirection * currentZoomValue;
            mainCamera.transform.LookAt(this.transform.position);
        }


        void Update()
        {
            if (leftMouseButtonIsPressed)
            {
                MoveByDraggingMouse();
            }
            else
            {
                SmoothMovement();
            }

            if (rightMouseButtonIsPressed)
            {
                HandleRotation();
            }

            HandleZooming();
        }

        private void HandleRotation()
        {
            Vector2 currentMousePos = this.inputManager.GetMousePosition();
            if (eventSystem.IsPointerOverGameObject())
            {
                rightMouseButtonClickPosition = currentMousePos;
                return;
            }

            var xNormalizedRotationDelta = (rightMouseButtonClickPosition.x - currentMousePos.x) / mainCamera.pixelWidth;
            float xRotationOffset = xNormalizedRotationDelta                                     * rotationMultiplier;

            var yNormalizedRotationDelta = (rightMouseButtonClickPosition.y - currentMousePos.y) / mainCamera.pixelWidth;
            float yRotationOffset = yNormalizedRotationDelta                                     * rotationMultiplier;


            this.transform.rotation *= Quaternion.Euler(yRotationOffset, -xRotationOffset, 0);
            ClampRotation();
            rightMouseButtonClickPosition = currentMousePos;
        }

        private void ClampRotation()
        {
        
            var rotation = this.transform.rotation;
            var rotationEulerAngles = rotation.eulerAngles;
            rotationEulerAngles.z = 0f;
            rotationEulerAngles.x = GetClampedXAngle(rotationEulerAngles.x);
        
            this.transform.rotation = Quaternion.Euler(rotationEulerAngles);
        }

        private float GetClampedXAngle(float xAngle)
        {
            const float xMaxAngle = 45;
            const float convertedXMinAngle = 360 - 52;
        
            if (xAngle is <= xMaxAngle or >= convertedXMinAngle)
            {
                return xAngle;
            }

            float distToMax = Mathf.Abs(xAngle - xMaxAngle);
            float distToMin = Mathf.Abs(xAngle - convertedXMinAngle);
            return distToMax < distToMin ? xMaxAngle : convertedXMinAngle;
        }
    

        void MoveByDraggingMouse()
        {
            if (eventSystem.IsPointerOverGameObject())
            {
                leftMouseButtonClickWorldPosition = GetMouseInWorldPosition();
                return;
            }

            targetPosition += leftMouseButtonClickWorldPosition - GetMouseInWorldPosition();
            ClampTargetPosition();
            transform.position = targetPosition;
        }


        Vector3 GetMouseInWorldPosition()
        {
            Plane mousePlane = new Plane(Vector3.up, Vector3.zero);
            Ray mouseRay = mainCamera.ScreenPointToRay(inputManager.GetMousePosition());

            if (mousePlane.Raycast(mouseRay, out float enter))
            {
                return mouseRay.GetPoint(enter);
            }

            return Vector3.zero;
        }


        void SmoothMovement()
        {
            var movementInputVector = inputManager.GetMove();
            if (enablePanByMouseAtBorder && movementInputVector == Vector2.zero)
            {
                movementInputVector = GetCamMoveInputUsingMousePosAndBorder();
            }

            var directionOffset = new Vector3(movementInputVector.x, 0, movementInputVector.y) * panSpeed * Time.unscaledDeltaTime;
            var offsetWithCameraRotation = Quaternion.Euler(0, transform.eulerAngles.y, 0)     * directionOffset;

            targetPosition += offsetWithCameraRotation;
            ClampTargetPosition();

            transform.position = Vector3.Lerp(transform.position, targetPosition, smoothingSpeed * Time.unscaledDeltaTime);
        }


        Vector2 GetCamMoveInputUsingMousePosAndBorder()
        {
            Vector2 result = new Vector2();

            var mousePos = inputManager.GetMousePosition();
            if (mousePos.x < panBorderThickness)
            {
                result.x = -1;
            }
            else if (mousePos.x > Screen.width - panBorderThickness)
            {
                result.x = 1;
            }

            if (mousePos.y < panBorderThickness)
            {
                result.y = -1;
            }
            else if (mousePos.y > Screen.height - panBorderThickness)
            {
                result.y = 1;
            }

            return result.normalized;
        }


        void ClampTargetPosition()
        {
            float clampedX = Mathf.Clamp(targetPosition.x, camBounds.min.x, camBounds.max.x);
            float clampedY = Mathf.Clamp(targetPosition.y, camBounds.min.y, camBounds.max.y);
            float clampedZ = Mathf.Clamp(targetPosition.z, camBounds.min.z, camBounds.max.z);
            targetPosition = new Vector3(clampedX, clampedY, clampedZ);
        }


        float GetScrollValue()
        {
            var mouseScroll = inputManager.GetMouseScroll();
            if (mouseScroll > 0)
            {
                return -scrollSpeed;
            }

            if (mouseScroll < 0)
            {
                return scrollSpeed;
            }

            return 0;
        }


        private void HandleZooming()
        {
            if (eventSystem.IsPointerOverGameObject())
            {
                return;
            }

            static float getMin(Vector3 v) => Mathf.Min(v.x, v.y);
            static float getMax(Vector3 v) => Mathf.Max(v.x, v.y);

            currentZoomValue += GetScrollValue();
            currentZoomValue = Mathf.Clamp(currentZoomValue, getMin(zoomBounds), getMax(zoomBounds));

            mainCamera.transform.localPosition = (rigToCamDirection * currentZoomValue);
        }


        void SetCameraBounds()
        {
            Queue<Transform> elementsToCheck = new Queue<Transform>();
            elementsToCheck.Enqueue(transformToGenerateBoundsFrom);

            while (elementsToCheck.Count != 0)
            {
                Transform next = elementsToCheck.Dequeue();
                camBounds.Encapsulate(next.position);
                foreach (Transform child in next)
                {
                    elementsToCheck.Enqueue(child);
                }
            }

            camBounds.min -= boundsPadding;
            camBounds.max += boundsPadding;

            //camBounds.min += mainCamera.transform.position;
            //camBounds.max += mainCamera.transform.position;
        }


        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.grey;
            Gizmos.DrawWireCube(camBounds.center, camBounds.size);
        }
    }
}
