﻿using System;

using UnityEngine;

namespace Clay.Helper
{
    public static class Tags
    {
        public const string Resource   = "Resource";
        public const string PlayerBase = "PlayerBase";
        public const string Enemy      = "Enemy";
    }

    public static class Colors
    {
        public static readonly Color Player = new(0, 0.394f, 0.929f);
        public static readonly Color Enemy  = new(0.734f, 0.023f, 0.156f);

        public static readonly Color Caspium  = new(1, 0.429f, 0);
        public static readonly Color Blotrium = new(0.367f, 0, 1);
    }

    public enum ResourceType
    {
        Caspium,
        Blotrium
    }
}
