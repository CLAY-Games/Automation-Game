using System;

using UnityEditor;

using UnityEngine;

namespace Clay.Helper
{
    public class HelperSo : ScriptableObject
    {
        [SerializeField] private Texture2D blotriumIcon;
        [SerializeField] private Texture2D caspiumIcon;

        public Texture2D GetResourceIcon(ResourceType resourceType)
        {
            return resourceType switch
            {
                ResourceType.Caspium  => caspiumIcon,
                ResourceType.Blotrium => blotriumIcon,
                _                     => throw new ArgumentOutOfRangeException(nameof(resourceType), resourceType, null)
            };
        }
    }
}
