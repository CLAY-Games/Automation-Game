﻿using System.Collections.Generic;

using UnityEngine;

namespace Clay.Helper
{
    public static class CollectionExtensions
    {
        public static T PopBack<T>(this IList<T> theList) where T : Object
        {
            if (theList.Count == 0)
            {
                return default;
            }

            var elementToReturn = theList[theList.Count - 1];
            theList.RemoveAt(theList.Count - 1);

            return elementToReturn;
        }


        public static T PopFront<T>(this IList<T> theList) where T : Object
        {
            if (theList.Count == 0)
            {
                return default;
            }

            var elementToReturn = theList[0];
            theList.RemoveAt(0);

            return elementToReturn;
        }


        public static T Last<T>(this IList<T> theList) where T : Object
        {
            if (theList.Count == 0)
            {
                return default;
            }

            return theList[^1];
        }
    }
}
