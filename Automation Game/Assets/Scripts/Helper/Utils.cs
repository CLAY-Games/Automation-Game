﻿using System;

using UnityEngine;

namespace Clay.Helper
{
    public static class Utils
    {
        public static Color ResourceTypeToColor(ResourceType resourceType)
        {
            return resourceType switch
            {
                ResourceType.Caspium  => Colors.Caspium,
                ResourceType.Blotrium => Colors.Blotrium,
                _                     => throw new ArgumentOutOfRangeException(nameof(resourceType), resourceType, null)
            };
        }

        public static Vector3 GetPointOnBezierCurve(Vector3 startPoint, Vector3 endPoint, float height, float progress)
        {
            Vector3 midPoint = (startPoint + endPoint) / 2f;
            midPoint.y += height;

            float u = 1 - progress;
            float tt = progress * progress;
            float uu = u * u;
            Vector3 p = uu * startPoint;
            p += 2 * u * progress * midPoint;
            p += tt * endPoint;
            return p;
        }
    }
}
