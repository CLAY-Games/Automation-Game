﻿using UnityEngine;

namespace Clay
{
    [RequireComponent(typeof(ResourceStorage))]
    public class ResourceField : MonoBehaviour
    {
        private ResourceStorage resourceStorage;

        private void Awake()
        {
            resourceStorage = GetComponent<ResourceStorage>();
        }

        private void Start()
        {
            resourceStorage.OnStorageWasEmptied += () => Destroy(this.gameObject);
        }
    }
}
