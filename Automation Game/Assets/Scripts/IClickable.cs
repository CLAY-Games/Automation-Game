﻿namespace Clay
{
    public interface IClickable
    {
        public void Click();
    }
}
