using UnityEngine;
using UnityEngine.Events;

namespace Clay.UI
{
    [CreateAssetMenu(fileName = "UiManager.asset", menuName = "ScriptableObjects/UiManager")]
    public class UiManager : ScriptableObject
    {
        private UnityAction currentDisableUiCallback;
    
        public void TakenBottomBar(UnityAction disableUiCallback)
        {
            currentDisableUiCallback?.Invoke();
            currentDisableUiCallback = disableUiCallback;
        }
    }
}
