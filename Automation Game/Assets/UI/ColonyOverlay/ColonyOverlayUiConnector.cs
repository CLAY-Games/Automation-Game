﻿using UnityEngine;
using UnityEngine.UIElements;

namespace Clay.UI.ColonyOverlay
{
    [RequireComponent(typeof(UIDocument))]
    public class ColonyOverlayUiConnector : MonoBehaviour, IColonyOverlayUiConnector
    {

        public int UnitsAmount
        {
            set => unitAmountUi.text = value.ToString();
        }

        public int MaxUnits
        {
            set => maxUnitsUi.text = value.ToString();
        }

        public int BlotriumAmount
        {
            set => blotriumAmountUi.text = value.ToString();
        }

        public int CaspiumAmount
        {
            set => caspiumAmountUi.text = value.ToString();
        }
        
        
        struct UxmlIds
        {
            public const string BlotriumAmount = "blotrium-amount";
            public const string CaspiumAmount  = "caspium-amount";
            public const string UnitAmount     = "unit-amount";
            public const string MaxUnits       = "max-units";
        }

        private UIDocument uiDocument;

        private Label blotriumAmountUi;
        private Label caspiumAmountUi;
        private Label unitAmountUi;
        private Label maxUnitsUi;

        private void Awake()
        {
            uiDocument = GetComponent<UIDocument>();

            VisualElement root = uiDocument.rootVisualElement;

            blotriumAmountUi = root.Q<Label>(UxmlIds.BlotriumAmount);
            caspiumAmountUi = root.Q<Label>(UxmlIds.CaspiumAmount);
            unitAmountUi = root.Q<Label>(UxmlIds.UnitAmount);
            maxUnitsUi = root.Q<Label>(UxmlIds.MaxUnits);
        }
    }
}
