﻿namespace Clay.UI.ColonyOverlay
{
    public interface IColonyOverlayUiConnector
    {
        public int UnitsAmount    { set; }
        public int MaxUnits       { set; }
        public int BlotriumAmount { set; }
        public int CaspiumAmount  { set; }
    }
}
