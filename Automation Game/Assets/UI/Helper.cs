﻿using UnityEngine.UIElements;

namespace Clay.UI
{
    public static class Helper
    {
        public static void EnableUi(UIDocument uiDocument)
        {
            uiDocument.rootVisualElement.style.display = DisplayStyle.Flex;
        }

        
        public static void DisableUi(UIDocument uiDocument)
        {
            if (uiDocument != null && uiDocument.rootVisualElement?.style?.display != null)
            {
                uiDocument.rootVisualElement.style.display = DisplayStyle.None;
            }
        }

        /// <summary>
        /// Returns ratio of fraction to value as percentage.
        /// If value == 0, the return value is 100
        /// </summary>
        /// <example>
        /// When <paramref name="fraction"/> == <paramref name="value"/> returns 0
        /// </example>
        /// <param name="fraction">The part of the percentage</param>
        /// <param name="value">The value to compare the fraction against</param>
        /// <returns>Percentage of fraction to value</returns>
        public static float GetPercentage(float fraction, float value)
        {
            return value == 0 ? 100f : (fraction / value) * 100f;
        }
    }
}
