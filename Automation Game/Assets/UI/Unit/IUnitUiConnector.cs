﻿using Clay.Helper;

using UnityEngine;
using UnityEngine.Events;

namespace Clay.UI.Unit
{
    public interface IUnitUiConnector : IDisableable
    {
        public int Hp    { set; }
        public int MaxHp { set; }

        public Color  ResourceColor     { set; }
        public string ResourceTitle     { set; }
        public int    ResourcesAmount   { set; }
        public int    ResourcesCapacity { set; }

        public event UnityAction OnPreferredResourceTypeClicked;

        public Color   PreferredResourceTypeColor { set; }
        public string  PreferredResourceType      { set; }
        public Texture2D PreferredResourceIcon      { set; }


        public float Damage { set; }
        public float Range  { set; }
        public float Speed  { set; }
    }
}
