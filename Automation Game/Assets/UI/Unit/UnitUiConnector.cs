﻿using Clay.Helper;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

namespace Clay.UI.Unit
{
    public class UnitUiConnector : MonoBehaviour, IUnitUiConnector
    {
        private int resourcesAmount;
        private int resourcesMax;

        private int          healthAmount;
        private int          healthMax;

        public int ResourcesAmount
        {
            set
            {
                resourcesAmount = value;
                resourcesAmountUi.text = value.ToString();
                UpdateResourcesBar();
            }
        }

        public int ResourcesCapacity
        {
            set
            {
                resourcesMax = value;
                resourcesCapacityUi.text = value.ToString();
                UpdateResourcesBar();
            }
        }

        public event UnityAction OnPreferredResourceTypeClicked;

        public Color PreferredResourceTypeColor
        {
            set => PreferredResourceContainerUi.style.backgroundColor = value;
        }

        public string PreferredResourceType
        {
            set => PreferredResourceUi.text = value;
        }

        public Texture2D PreferredResourceIcon
        {
            set => PreferredResourceIconUi.style.backgroundImage = value;
        }


        private void UpdateResourcesBar()
        {
            float percent = Helper.GetPercentage(resourcesAmount, resourcesMax);
            resourcesBarFillUi.style.width = Length.Percent(percent);
        }

        public int Hp
        {
            set
            {
                healthAmount = value;
                healthAmountUi.text = value.ToString();
                UpdateHealthBar();
            }
        }

        public int MaxHp
        {
            set
            {
                healthMax = value;
                healthMaxUi.text = value.ToString();
                UpdateHealthBar();
            }
        }

        private void UpdateHealthBar()
        {
            float percent = Helper.GetPercentage(healthAmount, healthMax);
            healthBarFillUi.style.width = Length.Percent(percent);
        }

        public Color ResourceColor
        {
            set
            {
                resourcesBarTitleUi.style.color = value;
                resourcesAmountUi.style.color = value;
                resourcesBarFillUi.style.backgroundColor = value;
            }
        }

        public string ResourceTitle
        {
            set => resourcesBarTitleUi.text = value;
        }

        public float        Damage                { set => damageUi.text = value.ToString(); }
        public float        Range                 { set => rangeUi.text = value.ToString(); }
        public float        Speed                 { set => speedUi.text = value.ToString(); }

        public void EnableUi()
        {
            UiManager.TakenBottomBar(DisableUi);
            Helper.EnableUi(uiDocument);
        }

        public void DisableUi()
        {
            Helper.DisableUi(uiDocument);
        }


        [SerializeField] private UiManager  UiManager;
        [SerializeField] private UIDocument uiDocument;

        private struct UxmlIds
        {
            public const string ExitButton = "exit-button";

            public const string PreferredResourceContainer = "preferred-resource-container";
            public const string PreferredResource          = "preferred-resource";
            public const string PreferredResourceIcon      = "preferred-resource-icon";

            public const string ResourcesTitle    = "resources-title";
            public const string ResourcesBarFill  = "resources-fill";
            public const string ResourcesAmount   = "resources-value";
            public const string ResourcesCapacity = "resources-max";

            public const string HealthFill   = "health-fill";
            public const string HealthAmount = "health-value";
            public const string HealthMax    = "health-max";

            public const string Damage = "damage-value";
            public const string Range  = "range-value";
            public const string Speed  = "speed-value";
        }

        private Button exitButtonUi;

        private VisualElement PreferredResourceContainerUi;
        private Label         PreferredResourceUi;
        private VisualElement PreferredResourceIconUi;

        private Label         resourcesBarTitleUi;
        private VisualElement resourcesBarFillUi;
        private Label         resourcesAmountUi;
        private Label         resourcesCapacityUi;

        private VisualElement healthBarFillUi;
        private Label         healthAmountUi;
        private Label         healthMaxUi;

        private Label damageUi;
        private Label rangeUi;
        private Label speedUi;


        private void OnEnable()
        {
            uiDocument = GetComponent<UIDocument>();
            InitUiReferences();
            InitCallbacks();
        }

        private void InitCallbacks()
        {
            exitButtonUi.clicked += DisableUi;
            PreferredResourceContainerUi.RegisterCallback<ClickEvent>(_ => OnPreferredResourceTypeClicked?.Invoke());
        }

        private void InitUiReferences()
        {
            var root = uiDocument.rootVisualElement;

            exitButtonUi = root.Q<Button>(UxmlIds.ExitButton);

            PreferredResourceUi = root.Q<Label>(UxmlIds.PreferredResource);
            PreferredResourceContainerUi = root.Q<VisualElement>(UxmlIds.PreferredResourceContainer);
            PreferredResourceIconUi = root.Q<VisualElement>(UxmlIds.PreferredResourceIcon);

            resourcesBarTitleUi = root.Q<Label>(UxmlIds.ResourcesTitle);
            resourcesBarFillUi = root.Q<VisualElement>(UxmlIds.ResourcesBarFill);
            resourcesAmountUi = root.Q<Label>(UxmlIds.ResourcesAmount);
            resourcesCapacityUi = root.Q<Label>(UxmlIds.ResourcesCapacity);

            healthBarFillUi = root.Q<VisualElement>(UxmlIds.HealthFill);
            healthAmountUi = root.Q<Label>(UxmlIds.HealthAmount);
            healthMaxUi = root.Q<Label>(UxmlIds.HealthMax);

            damageUi = root.Q<Label>(UxmlIds.Damage);
            rangeUi = root.Q<Label>(UxmlIds.Range);
            speedUi = root.Q<Label>(UxmlIds.Speed);
        }
    }
}
