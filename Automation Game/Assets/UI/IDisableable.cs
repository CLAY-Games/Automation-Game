namespace Clay.UI
{
    public interface IDisableable
    {
        public void EnableUi();
        public void DisableUi();

    }
}
