﻿using UnityEngine;
using UnityEngine.UIElements;

namespace Clay.UI.Resource
{
    public interface IResourceUiConnector : IDisableable
    {
        public int             Remaining       { set; }
        public int             Capacity        { set; }
        public string          Title           { set; } 
        public Color ResourceColor      { set; }
    }
}
