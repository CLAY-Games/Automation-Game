using Clay.Helper;

namespace Clay.UI.Resource
{
    public interface IResourceUi
    {
        ResourceType ResourceType { set; }
        int          Capacity     { set; }
        int          Amount       { set; }

        void Disable();
        void Enable();
    }
}
