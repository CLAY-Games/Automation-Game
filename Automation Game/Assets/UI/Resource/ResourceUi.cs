﻿using System;

using Clay.Helper;
using Clay.Input;

using UnityEngine;
using UnityEngine.UIElements;

namespace Clay.UI.Resource
{
    [RequireComponent(typeof(IResourceUiConnector))]
    public class ResourceUi : MonoBehaviour, IResourceUi
    {
        [SerializeField] private InputManager inputManager;
        [SerializeField] private HelperSo     helperSo;

        private IResourceUiConnector uiConnector;

        public ResourceType ResourceType
        {
            set
            {
                uiConnector.ResourceColor = Utils.ResourceTypeToColor(value);
                uiConnector.Title = value.ToString();
            }
        }

        public int Capacity
        {
            set => uiConnector.Capacity = value;
        }

        public int Amount
        {
            set => uiConnector.Remaining = value;
        }

        public void Enable()
        {
            uiConnector.EnableUi();
        }

        public void Disable()
        {
            uiConnector.DisableUi();
        }

        private void Awake()
        {
            uiConnector = GetComponent<IResourceUiConnector>();
        }

        private void Start()
        {
            inputManager.DeselectAll += (_) => uiConnector.DisableUi();
        }
    }
}
