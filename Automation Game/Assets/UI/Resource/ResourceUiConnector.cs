﻿using UnityEngine;
using UnityEngine.UIElements;

namespace Clay.UI.Resource
{
    public class ResourceUiConnector : MonoBehaviour, IResourceUiConnector
    {
        private int remaining;
        private int capacity;
        
        public int Capacity
        {
            set
            {
                capacity = value;
                capacityUi.text = value.ToString();
                UpdateBar();
            }
        }

        public int Remaining
        {
            set
            {
                remaining = value;
                remainingUi.text = value.ToString();
                UpdateBar();
            }
        }

        private void UpdateBar()
        {
            float percentage = Helper.GetPercentage(remaining, capacity);
            resourceBarFillUi.style.width = Length.Percent(percentage);
        }

        public string Title { set => titleUi.text = value; }

        public Color ResourceColor
        {
            set
            {
                resourceBarFillUi.style.backgroundColor = value;
                titleUi.style.color = value;
                remainingUi.style.color = value;
            }
        }

        public void EnableUi()
        {
            uiManager.TakenBottomBar(DisableUi);
            Helper.EnableUi(uiDocument);
        }

        public void DisableUi()
        {
            Helper.DisableUi(uiDocument);
        }

        [SerializeField] private UiManager  uiManager;
        [SerializeField] private UIDocument uiDocument;

        private struct UxmlIds
        {
            public const string ExitButton      = "exit-button";
            public const string ResourceBarFill = "resource-fill";
            public const string ResourceTitle   = "resource-title";
            public const string Capacity        = "resource-max";
            public const string Remaining       = "resource-value";
        }

        private Button        exitButtonUi;
        private VisualElement resourceBarFillUi;
        private Label         titleUi;
        private Label         capacityUi;
        private Label         remainingUi;

        private void OnEnable()
        {
            uiDocument = GetComponent<UIDocument>();
            InitUiReferences();
            InitCallbacks();
        }

        private void InitUiReferences()
        {
            var root = uiDocument.rootVisualElement;

            exitButtonUi = root.Q<Button>(UxmlIds.ExitButton);
            resourceBarFillUi = root.Q<VisualElement>(UxmlIds.ResourceBarFill);
            titleUi = root.Q<Label>(UxmlIds.ResourceTitle);
            capacityUi = root.Q<Label>(UxmlIds.Capacity);
            remainingUi = root.Q<Label>(UxmlIds.Remaining);
        }

        private void InitCallbacks()
        {
            exitButtonUi.clicked += DisableUi;
        }
    }
}
