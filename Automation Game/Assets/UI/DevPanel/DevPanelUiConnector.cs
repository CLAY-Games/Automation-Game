using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

namespace Clay.UI.DevPanel
{
    [RequireComponent(typeof(UIDocument))]
    public class DevPanelUiConnector : MonoBehaviour, IDevPanelUiConnector
    {
        struct UxmlIds
        {
            public const string SpawnPlayerUnitButton = "spawn-player-unit-button";
            public const string SpawnEnemyUnitButton  = "spawn-enemy-unit-button";
            public const string TimeScaleSlider       = "time-scale-slider";
            public const string CameraPanToggle       = "camera-pan-toggle";
        }

        public event UnityAction        SpawnPlayerUnit;
        public event UnityAction        SpawnEnemyUnit;
        public event UnityAction<float> TimeScaleChanged;
        public event UnityAction<bool>  CameraPanToggleChanged;

        private UIDocument uiDocument;

        private Toggle cameraPanToggle;

        private void Awake()
        {
            uiDocument = GetComponent<UIDocument>();

            VisualElement root = uiDocument.rootVisualElement;
            Button spawnPlayerUnitButton = root.Q<Button>(UxmlIds.SpawnPlayerUnitButton);
            spawnPlayerUnitButton.clicked += () => SpawnPlayerUnit?.Invoke();

            Button spawnEnemyUnitButton = root.Q<Button>(UxmlIds.SpawnEnemyUnitButton);
            spawnEnemyUnitButton.clicked += () => SpawnEnemyUnit?.Invoke();

            Slider timeScaleSlider = root.Q<Slider>(UxmlIds.TimeScaleSlider);
            timeScaleSlider.RegisterValueChangedCallback(value => TimeScaleChanged?.Invoke(value.newValue));

            Toggle camPanToggle = root.Q<Toggle>(UxmlIds.CameraPanToggle);
            camPanToggle.RegisterValueChangedCallback(isOn => CameraPanToggleChanged?.Invoke(isOn.newValue));
        }

        public void EnableUi()
        {
            Helper.EnableUi(uiDocument);
        }

        public void DisableUi()
        {
            Helper.DisableUi(uiDocument);
        }
    }
}
