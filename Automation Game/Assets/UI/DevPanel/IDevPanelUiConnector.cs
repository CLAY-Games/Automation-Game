using UnityEngine.Events;

namespace Clay.UI.DevPanel
{
    public interface IDevPanelUiConnector : IDisableable
    {
        public event UnityAction SpawnPlayerUnit;
        public event UnityAction SpawnEnemyUnit;

        public event UnityAction<float> TimeScaleChanged;
        public event UnityAction<bool>  CameraPanToggleChanged;
    }
}
