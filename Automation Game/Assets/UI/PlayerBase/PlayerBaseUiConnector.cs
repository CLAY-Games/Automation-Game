﻿using System;

using UnityEngine;
using UnityEngine.UIElements;

namespace Clay.UI.PlayerBase
{
    public class PlayerBaseUiConnector : MonoBehaviour, IPlayerBaseUiConnector
    {
        public event Action OnBuildNewUnit;

        public UiManager UiManager;

        private int caspiumAmount;
        private int caspiumCapacity;

        private int blotriumAmount;
        private int blotriumCapacity;

        public int CaspiumCapacity
        {
            set
            {
                caspiumCapacity = value;
                caspiumCapacityUi.text = value.ToString();
                UpdateCaspiumBar();
            }
        }

        public int CaspiumAmount
        {
            set
            {
                caspiumAmount = value;
                caspiumAmountUi.text = value.ToString();
                UpdateCaspiumBar();
            }
        }

        private void UpdateCaspiumBar()
        {
            float percent = Helper.GetPercentage(caspiumAmount, caspiumCapacity);
            caspiumBarFillUi.style.width = Length.Percent(percent);
        }

        public int BlotriumCapacity
        {
            set
            {
                blotriumCapacity = value;
                blotriumCapacityUi.text = value.ToString();
                UpdateBlotriumBar();
            }
        }

        public int BlotriumAmount
        {
            set
            {
                blotriumAmount = value;
                blotriumAmountUi.text = value.ToString();
                UpdateBlotriumBar();
            }
        }

        private void UpdateBlotriumBar()
        {
            float percent = Helper.GetPercentage(blotriumAmount, blotriumCapacity);
            blotriumBarFillUi.style.width = Length.Percent(percent);
        }

        float IPlayerBaseUiConnector.TimeUntilNextUnit
        {
            set
            {
                timeUntilNextUnit = value;
                UpdateUnitQueueUi();
            }
        }

        private void UpdateUnitQueueUi()
        {
            timeUntilNextUnitUi.text = timeUntilNextUnit.ToString("F1");
            // unitProgressBarUi.lowValue = totalTimeForNextUnit - timeUntilNextUnit;
        }

        // TODO evaluate whether to keep or scrap the unit queue progress bar
        float IPlayerBaseUiConnector.TotalTimeForNextUnit
        {
            get;
            set;
        }

        public int NrUnitsInProduction
        {
            set => nrUnitsInProductionUi.text = value.ToString();
        }

        public int UnitPrice
        {
            set => buildNewUnitButtonUi.text = $"Queue unit ({value})";
        }

        struct UxmlIds
        {
            public const string ExitButton = "exit-button";

            // public const string UnitQueueProgressBar = "unit-queue-progress-bar";
            public const string TimeUntilNextUnit   = "timer-value";
            public const string NrUnitsInProduction = "queue-value";
            public const string BuildNewUnitButton  = "unit-button";

            public const string CaspiumFill     = "caspium-fill";
            public const string CaspiumCapacity = "caspium-max";
            public const string CaspiumAmount   = "caspium-value";

            public const string BlotriumFill     = "blotrium-fill";
            public const string BlotriumCapacity = "blotrium-max";
            public const string BlotriumAmount   = "blotrium-value";
        }

        private Button exitButtonUi;

        private VisualElement caspiumBarFillUi;
        private Label         caspiumAmountUi;
        private Label         caspiumCapacityUi;

        private VisualElement blotriumBarFillUi;
        private Label         blotriumAmountUi;
        private Label         blotriumCapacityUi;

        // private ProgressBar unitProgressBarUi;
        private Label  timeUntilNextUnitUi;
        private Label  nrUnitsInProductionUi;
        private Button buildNewUnitButtonUi;

        private UIDocument uiDocument;

        private float timeUntilNextUnit;

        void OnEnable()
        {
            InitUiProperties();
            RegisterEvents();
        }

        private void InitUiProperties()
        {
            uiDocument = GetComponent<UIDocument>();
            VisualElement root = uiDocument.rootVisualElement;

            exitButtonUi = root.Q<Button>(UxmlIds.ExitButton);

            caspiumBarFillUi = root.Q<VisualElement>(UxmlIds.CaspiumFill);
            caspiumAmountUi = root.Q<Label>(UxmlIds.CaspiumAmount);
            caspiumCapacityUi = root.Q<Label>(UxmlIds.CaspiumCapacity);

            blotriumBarFillUi = root.Q<VisualElement>(UxmlIds.BlotriumFill);
            blotriumAmountUi = root.Q<Label>(UxmlIds.BlotriumAmount);
            blotriumCapacityUi = root.Q<Label>(UxmlIds.BlotriumCapacity);

            // unitProgressBarUi = root.Q<ProgressBar>(UxmlIds.UnitQueueProgressBar);
            nrUnitsInProductionUi = root.Q<Label>(UxmlIds.NrUnitsInProduction);
            timeUntilNextUnitUi = root.Q<Label>(UxmlIds.TimeUntilNextUnit);
            buildNewUnitButtonUi = root.Q<Button>(UxmlIds.BuildNewUnitButton);
        }

        private void RegisterEvents()
        {
            buildNewUnitButtonUi.clicked += () => OnBuildNewUnit?.Invoke();
            exitButtonUi.clicked += OnExitButtonUiPressed;
        }

        public void EnableUi()
        {
            UiManager.TakenBottomBar(DisableUi);
            Helper.EnableUi(uiDocument);
        }

        public void DisableUi()
        {
            Helper.DisableUi(uiDocument);
        }

        void OnExitButtonUiPressed()
        {
            DisableUi();
        }
    }
}
