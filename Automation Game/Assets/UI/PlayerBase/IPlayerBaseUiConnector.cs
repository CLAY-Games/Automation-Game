﻿using System;

namespace Clay.UI.PlayerBase
{
    public interface IPlayerBaseUiConnector : IDisableable
    {
        public event Action OnBuildNewUnit;

        public int CaspiumCapacity  { set; }
        public int CaspiumAmount    { set; }
        public int BlotriumCapacity { set; }
        public int BlotriumAmount   { set; }

        public float TimeUntilNextUnit { set; }

        // TODO evaluate whether to keep or scrap the unit queue progress bar
        public float TotalTimeForNextUnit { protected get; set; }
        public int   NrUnitsInProduction  { set; }
        public int   UnitPrice            { set; }
    }
}
