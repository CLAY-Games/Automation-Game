using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Clay.Input
{
    [CreateAssetMenu(fileName = "InputManager.asset", menuName = "ScriptableObjects/InputManager")]
    public class InputManager : ScriptableObject
    {
        public event UnityAction<InputAction.CallbackContext> OnClickPerformed;
        public event UnityAction<InputAction.CallbackContext> OnClickStarted;
        public event UnityAction<InputAction.CallbackContext> OnClickCanceled;
        public event UnityAction<InputAction.CallbackContext> OnClickRmbPerformed;
        public event UnityAction<InputAction.CallbackContext> OnClickRmbStarted;
        public event UnityAction<InputAction.CallbackContext> OnClickRmbCanceled;
        public event UnityAction<InputAction.CallbackContext> DeselectAll;
        public event UnityAction<InputAction.CallbackContext> OnToggleDevPanel;

        public Vector2 GetMousePosition()
        {
            return controls.Default.MousePos.ReadValue<Vector2>();
        }

        public float GetMouseScroll() => controls.Default.MouseScroll.ReadValue<float>();

        public Vector2 GetMove() => controls.Default.Move.ReadValue<Vector2>();


        private Controls controls;


        private void OnEnable()
        {
            if (controls == null)
            {
                controls = new Controls();
                RegisterCallbacks();
            }

            controls.Default.Enable();
        }

        private void RegisterCallbacks()
        {
            controls.Default.Click.performed += ctx => OnClickPerformed?.Invoke(ctx);
            controls.Default.Click.started += ctx => OnClickStarted?.Invoke(ctx);
            controls.Default.Click.canceled += ctx => OnClickCanceled?.Invoke(ctx);
        
            controls.Default.ClickRmb.performed += ctx => OnClickRmbPerformed?.Invoke(ctx);
            controls.Default.ClickRmb.started += ctx => OnClickRmbStarted?.Invoke(ctx);
            controls.Default.ClickRmb.canceled += ctx => OnClickRmbCanceled?.Invoke(ctx);

            controls.Default.DeselectAll.performed += ctx => DeselectAll?.Invoke(ctx);
            controls.Default.ToggleDevPanel.performed += ctx => OnToggleDevPanel?.Invoke(ctx);
        }

        private void OnDisable()
        {
            controls.Disable();
        }
    }
}
